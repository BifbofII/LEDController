# Generated by YCM Generator at 2018-02-18 17:09:44.846401

# This file is NOT licensed under the GPLv3, which is the license for the rest
# of YouCompleteMe.
#
# Here's the license text for this file:
#
# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

import os
import ycm_core

flags = [
    '-x', 'c++',
    '-D',
    '-DCONFIGURED',
    '-DCURSES_LOC=<ncurses.h>',
    '-DEMBEDDED_SUPP',
    '-DESP_PLATFORM',
    '-DHAVE_CONFIG_H',
    '-DHAVE_EXPAT_CONFIG_H',
    '-DHAVE_WEAK_SYMBOLS',
    '-DLOCALE',
    '-DMBEDTLS_CONFIG_FILE="mbedtls/esp_config.h"',
    '-DNATIVE_LITTLE_ENDIAN',
    '-DNCURSES_WIDECHAR=1',
    '-DRANDOMBYTES_DEFAULT_IMPLEMENTATION',
    '-DWITH_POSIX',
    '-D_DEFAULT_SOURCE',
    '-D_GNU_SOURCE',
    '-D__STDC_CONSTANT_MACROS',
    '-D__STDC_LIMIT_MACROS',
    '-D__ets__',
    '-I/home/christoph/Git/LEDController/build/bootloader/../include',
    '-Wall',
    '-Werror=all',
    '-Wextra',
    '-Wno-address',
    '-Wno-error=deprecated-declarations',
    '-Wno-error=unused-but-set-variable',
    '-Wno-error=unused-function',
    '-Wno-error=unused-variable',
    '-Wno-old-style-declaration',
    '-Wno-sign-compare',
    '-Wno-strict-aliasing',
    '-Wno-type-limits',
    '-Wno-unknown-pragmas',
    '-Wno-unused-but-set-variable',
    '-Wno-unused-function',
    '-Wno-unused-parameter',
    '-Wno-unused-variable',
    '-nostdlib',
    '-std=c++11',
    '-I', '.',
    '-I', '../include',
    '-I', '/home/christoph/ESP/esp-idf/components/app_trace/include',
    '-I', '/home/christoph/ESP/esp-idf/components/app_update/include',
    '-I', '/home/christoph/ESP/esp-idf/components/bootloader/subproject/main/include',
    '-I', '/home/christoph/ESP/esp-idf/components/bootloader_support/include',
    '-I', '/home/christoph/ESP/esp-idf/components/bootloader_support/include_priv',
    '-I', '/home/christoph/ESP/esp-idf/components/bt/include',
    '-I', '/home/christoph/ESP/esp-idf/components/coap/libcoap/include',
    '-I', '/home/christoph/ESP/esp-idf/components/coap/libcoap/include/coap',
    '-I', '/home/christoph/ESP/esp-idf/components/coap/port/include',
    '-I', '/home/christoph/ESP/esp-idf/components/coap/port/include/coap',
    '-I', '/home/christoph/ESP/esp-idf/components/console',
    '-I', '/home/christoph/ESP/esp-idf/components/console/.',
    '-I', '/home/christoph/ESP/esp-idf/components/cxx/include',
    '-I', '/home/christoph/ESP/esp-idf/components/driver/include',
    '-I', '/home/christoph/ESP/esp-idf/components/driver/include/driver',
    '-I', '/home/christoph/ESP/esp-idf/components/esp32/include',
    '-I', '/home/christoph/ESP/esp-idf/components/esp_adc_cal/include',
    '-I', '/home/christoph/ESP/esp-idf/components/ethernet/include',
    '-I', '/home/christoph/ESP/esp-idf/components/expat/include/expat',
    '-I', '/home/christoph/ESP/esp-idf/components/expat/port/include',
    '-I', '/home/christoph/ESP/esp-idf/components/fatfs/src',
    '-I', '/home/christoph/ESP/esp-idf/components/freertos/include',
    '-I', '/home/christoph/ESP/esp-idf/components/freertos/include/freertos',
    '-I', '/home/christoph/ESP/esp-idf/components/heap/include',
    '-I', '/home/christoph/ESP/esp-idf/components/idf_test/include',
    '-I', '/home/christoph/ESP/esp-idf/components/jsmn/include',
    '-I', '/home/christoph/ESP/esp-idf/components/jsmn/include/',
    '-I', '/home/christoph/ESP/esp-idf/components/json/cJSON',
    '-I', '/home/christoph/ESP/esp-idf/components/libsodium/libsodium/src/libsodium/include',
    '-I', '/home/christoph/ESP/esp-idf/components/libsodium/libsodium/src/libsodium/include/sodium',
    '-I', '/home/christoph/ESP/esp-idf/components/libsodium/port',
    '-I', '/home/christoph/ESP/esp-idf/components/libsodium/port_include',
    '-I', '/home/christoph/ESP/esp-idf/components/libsodium/port_include/sodium',
    '-I', '/home/christoph/ESP/esp-idf/components/log/include',
    '-I', '/home/christoph/ESP/esp-idf/components/lwip/apps/ping',
    '-I', '/home/christoph/ESP/esp-idf/components/lwip/include/lwip',
    '-I', '/home/christoph/ESP/esp-idf/components/lwip/include/lwip/port',
    '-I', '/home/christoph/ESP/esp-idf/components/lwip/include/lwip/posix',
    '-I', '/home/christoph/ESP/esp-idf/components/mbedtls/include',
    '-I', '/home/christoph/ESP/esp-idf/components/mbedtls/port/include',
    '-I', '/home/christoph/ESP/esp-idf/components/mdns/include',
    '-I', '/home/christoph/ESP/esp-idf/components/mdns/private_include',
    '-I', '/home/christoph/ESP/esp-idf/components/micro-ecc/micro-ecc',
    '-I', '/home/christoph/ESP/esp-idf/components/newlib/include',
    '-I', '/home/christoph/ESP/esp-idf/components/newlib/platform_include',
    '-I', '/home/christoph/ESP/esp-idf/components/nghttp/nghttp2/lib/includes',
    '-I', '/home/christoph/ESP/esp-idf/components/nghttp/port/include',
    '-I', '/home/christoph/ESP/esp-idf/components/nvs_flash/include',
    '-I', '/home/christoph/ESP/esp-idf/components/openssl/include',
    '-I', '/home/christoph/ESP/esp-idf/components/openssl/include/internal',
    '-I', '/home/christoph/ESP/esp-idf/components/openssl/include/openssl',
    '-I', '/home/christoph/ESP/esp-idf/components/openssl/include/platform',
    '-I', '/home/christoph/ESP/esp-idf/components/pthread/include',
    '-I', '/home/christoph/ESP/esp-idf/components/sdmmc/include',
    '-I', '/home/christoph/ESP/esp-idf/components/soc/esp32/include',
    '-I', '/home/christoph/ESP/esp-idf/components/soc/include',
    '-I', '/home/christoph/ESP/esp-idf/components/spi_flash/include',
    '-I', '/home/christoph/ESP/esp-idf/components/spiffs/include',
    '-I', '/home/christoph/ESP/esp-idf/components/spiffs/spiffs/src',
    '-I', '/home/christoph/ESP/esp-idf/components/tcpip_adapter/include',
    '-I', '/home/christoph/ESP/esp-idf/components/ulp/include',
    '-I', '/home/christoph/ESP/esp-idf/components/vfs/include',
    '-I', '/home/christoph/ESP/esp-idf/components/wear_levelling/include',
    '-I', '/home/christoph/ESP/esp-idf/components/wpa_supplicant/../esp32/include',
    '-I', '/home/christoph/ESP/esp-idf/components/wpa_supplicant/include',
    '-I', '/home/christoph/ESP/esp-idf/components/wpa_supplicant/port/include',
    '-I', '/home/christoph/ESP/esp-idf/components/xtensa-deb-std=gnu99',
    '-I', '/home/christoph/ESP/esp-idf/components/xtensa-debug-module/include',
    '-I', '/home/christoph/Git/LEDController/build/bootloader/include',
    '-I', '/home/christoph/Git/LEDController/build/include',
    '-I', '/home/christoph/Git/LEDController/components/rgb_led/include',
    '-I', '/home/christoph/Git/LEDController/main/include',
    '-I', 'api',
    '-I', 'apps',
    '-I', 'apps/ping',
    '-I', 'apps/sntp',
    '-I', 'argtable3',
    '-I', 'cJSON',
    '-I', 'core',
    '-I', 'core/ipv4',
    '-I', 'core/ipv6',
    '-I', 'esp32',
    '-I', 'eth_phy',
    '-I', 'gcov',
    '-I', 'hwcrypto',
    '-I', 'libcoap/src',
    '-I', 'library',
    '-I', 'libsodium/src/libsodium/crypto_aead/chacha20poly1305/sodium',
    '-I', 'libsodium/src/libsodium/crypto_aead/xchacha20poly1305/sodium',
    '-I', 'libsodium/src/libsodium/crypto_auth',
    '-I', 'libsodium/src/libsodium/crypto_auth/hmacsha256',
    '-I', 'libsodium/src/libsodium/crypto_auth/hmacsha512',
    '-I', 'libsodium/src/libsodium/crypto_auth/hmacsha512256',
    '-I', 'libsodium/src/libsodium/crypto_box',
    '-I', 'libsodium/src/libsodium/crypto_box/curve25519xsalsa20poly1305',
    '-I', 'libsodium/src/libsodium/crypto_core/curve25519/ref10',
    '-I', 'libsodium/src/libsodium/crypto_core/hchacha20',
    '-I', 'libsodium/src/libsodium/crypto_core/hsalsa20',
    '-I', 'libsodium/src/libsodium/crypto_core/hsalsa20/ref2',
    '-I', 'libsodium/src/libsodium/crypto_core/salsa/ref',
    '-I', 'libsodium/src/libsodium/crypto_generichash',
    '-I', 'libsodium/src/libsodium/crypto_generichash/blake2b',
    '-I', 'libsodium/src/libsodium/crypto_generichash/blake2b/ref',
    '-I', 'libsodium/src/libsodium/crypto_hash',
    '-I', 'libsodium/src/libsodium/crypto_hash/sha256',
    '-I', 'libsodium/src/libsodium/crypto_hash/sha256/cp',
    '-I', 'libsodium/src/libsodium/crypto_hash/sha512',
    '-I', 'libsodium/src/libsodium/crypto_hash/sha512/cp',
    '-I', 'libsodium/src/libsodium/crypto_kdf',
    '-I', 'libsodium/src/libsodium/crypto_kdf/blake2b',
    '-I', 'libsodium/src/libsodium/crypto_kx',
    '-I', 'libsodium/src/libsodium/crypto_onetimeauth',
    '-I', 'libsodium/src/libsodium/crypto_onetimeauth/poly1305',
    '-I', 'libsodium/src/libsodium/crypto_onetimeauth/poly1305/donna',
    '-I', 'libsodium/src/libsodium/crypto_pwhash',
    '-I', 'libsodium/src/libsodium/crypto_pwhash/argon2',
    '-I', 'libsodium/src/libsodium/crypto_pwhash/scryptsalsa208sha256',
    '-I', 'libsodium/src/libsodium/crypto_pwhash/scryptsalsa208sha256/nosse',
    '-I', 'libsodium/src/libsodium/crypto_scalarmult',
    '-I', 'libsodium/src/libsodium/crypto_scalarmult/curve25519',
    '-I', 'libsodium/src/libsodium/crypto_scalarmult/curve25519/ref10',
    '-I', 'libsodium/src/libsodium/crypto_secretbox',
    '-I', 'libsodium/src/libsodium/crypto_secretbox/xsalsa20poly1305',
    '-I', 'libsodium/src/libsodium/crypto_shorthash',
    '-I', 'libsodium/src/libsodium/crypto_shorthash/siphash24',
    '-I', 'libsodium/src/libsodium/crypto_shorthash/siphash24/ref',
    '-I', 'libsodium/src/libsodium/crypto_sign',
    '-I', 'libsodium/src/libsodium/crypto_sign/ed25519',
    '-I', 'libsodium/src/libsodium/crypto_sign/ed25519/ref10',
    '-I', 'libsodium/src/libsodium/crypto_stream',
    '-I', 'libsodium/src/libsodium/crypto_stream/chacha20',
    '-I', 'libsodium/src/libsodium/crypto_stream/chacha20/ref',
    '-I', 'libsodium/src/libsodium/crypto_stream/salsa20',
    '-I', 'libsodium/src/libsodium/crypto_stream/salsa20/ref',
    '-I', 'libsodium/src/libsodium/crypto_stream/xsalsa20',
    '-I', 'libsodium/src/libsodium/crypto_verify/sodium',
    '-I', 'libsodium/src/libsodium/randombytes',
    '-I', 'libsodium/src/libsodium/sodium',
    '-I', 'linenoise',
    '-I', 'micro-ecc',
    '-I', 'netif',
    '-I', 'nghttp2/lib',
    '-I', 'platform',
    '-I', 'port',
    '-I', 'port/debug',
    '-I', 'port/freertos',
    '-I', 'port/netif',
    '-I', 'spiffs/src',
    '-I', 'src',
    '-I', 'src/',
    '-I', 'src/crypto',
    '-I', 'src/fast_crypto',
]


# Set this to the absolute path to the folder (NOT the file!) containing the
# compile_commands.json file to use that instead of 'flags'. See here for
# more details: http://clang.llvm.org/docs/JSONCompilationDatabase.html
#
# You can get CMake to generate this file for you by adding:
#   set( CMAKE_EXPORT_COMPILE_COMMANDS 1 )
# to your CMakeLists.txt file.
#
# Most projects will NOT need to set this to anything; you can just change the
# 'flags' list of compilation flags. Notice that YCM itself uses that approach.
compilation_database_folder = ''

if os.path.exists( compilation_database_folder ):
  database = ycm_core.CompilationDatabase( compilation_database_folder )
else:
  database = None

SOURCE_EXTENSIONS = [ '.C', '.cpp', '.cxx', '.cc', '.c', '.m', '.mm' ]

def DirectoryOfThisScript():
  return os.path.dirname( os.path.abspath( __file__ ) )


def MakeRelativePathsInFlagsAbsolute( flags, working_directory ):
  if not working_directory:
    return list( flags )
  new_flags = []
  make_next_absolute = False
  path_flags = [ '-isystem', '-I', '-iquote', '--sysroot=' ]
  for flag in flags:
    new_flag = flag

    if make_next_absolute:
      make_next_absolute = False
      if not flag.startswith( '/' ):
        new_flag = os.path.join( working_directory, flag )

    for path_flag in path_flags:
      if flag == path_flag:
        make_next_absolute = True
        break

      if flag.startswith( path_flag ):
        path = flag[ len( path_flag ): ]
        new_flag = path_flag + os.path.join( working_directory, path )
        break

    if new_flag:
      new_flags.append( new_flag )
  return new_flags


def IsHeaderFile( filename ):
  extension = os.path.splitext( filename )[ 1 ]
  return extension in [ '.H', '.h', '.hxx', '.hpp', '.hh' ]


def GetCompilationInfoForFile( filename ):
  # The compilation_commands.json file generated by CMake does not have entries
  # for header files. So we do our best by asking the db for flags for a
  # corresponding source file, if any. If one exists, the flags for that file
  # should be good enough.
  if IsHeaderFile( filename ):
    basename = os.path.splitext( filename )[ 0 ]
    for extension in SOURCE_EXTENSIONS:
      replacement_file = basename + extension
      if os.path.exists( replacement_file ):
        compilation_info = database.GetCompilationInfoForFile(
          replacement_file )
        if compilation_info.compiler_flags_:
          return compilation_info
    return None
  return database.GetCompilationInfoForFile( filename )


def FlagsForFile( filename, **kwargs ):
  if database:
    # Bear in mind that compilation_info.compiler_flags_ does NOT return a
    # python list, but a "list-like" StringVec object
    compilation_info = GetCompilationInfoForFile( filename )
    if not compilation_info:
      return None

    final_flags = MakeRelativePathsInFlagsAbsolute(
      compilation_info.compiler_flags_,
      compilation_info.compiler_working_dir_ )

  else:
    relative_to = DirectoryOfThisScript()
    final_flags = MakeRelativePathsInFlagsAbsolute( flags, relative_to )

  return {
    'flags': final_flags,
    'do_cache': True
  }

