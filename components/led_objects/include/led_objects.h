/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    led_objects.h      = */
/* =============================== */

#ifndef LED_OBJECTS
#define LED_OBJECTS

// ESP-IDF libraries
#include "driver/ledc.h"
#include "esp_log.h"

// C/C++ libraries
#include <math.h>
#include <stdexcept>
#include <vector>

namespace LED {
ledc_timer_config_t
initTimer(ledc_timer_bit_t res = LEDC_TIMER_10_BIT, uint32_t freq = 5000,
          ledc_mode_t mode = LEDC_HIGH_SPEED_MODE,
          ledc_timer_t num =
              LEDC_TIMER_0); // Initialize the timer for the LED controllers

struct controller_channel {
  ledc_channel_t channelCode;
  bool used;
};

class LED {
protected:
  struct led_channel {
    int pin;
    ledc_channel_t controllerChannel;
    double correctionFactor;
    double color;
  };

  std::vector<led_channel> channel;

  ledc_timer_t timer;
  ledc_mode_t speedMode;
  ledc_timer_bit_t resolution;

  bool enabled = false;
  double brightness = 1;
  int fadeTime = 0;

  void writeOutput();     // Write the output to the hardware
  void initLEDChannels(); // Set up the LED channels
public:
  LED(ledc_timer_config_t timerConf, int _pin, double _correctionFactor = 1);
  ~LED();
  void turnOn();                          // Turn on the LED
  void turnOff();                         // Turn off the LED
  void setEnabled(bool _enabled);         // Set the enable state of the LED
  bool toggle();                          // Toggle status of the LED
  void setBrightness(double _brightness); // Set brightness of the LED
  void setFadeTime(int _fadeTime);        // Set fade time of the LED
  bool getEnabled();                      // Get enabled state
};

class RGBLed : public LED {
public:
  RGBLed(ledc_timer_config_t timerConf, int _pinRed, int _pinGreen,
         int _pinBlue, double _correctRed = 1, double _correctGreen = 1,
         double _correctBlue = 1);
  void setColor(int hexColor,
                bool enable = false); // Set color via hexadecimal value
  void setColor(double red, double green, double blue,
                bool enable = false); // Set color via 0-1 values
};
} // namespace LED

#endif
