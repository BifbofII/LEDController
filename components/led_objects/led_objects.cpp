/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    led_objects.cpp    = */
/* =============================== */

#include "led_objects.h"

namespace LED {
static const char *TAG = "LED"; // Logging tag

controller_channel channelOverview[] = {
    // Used to keep track of used channels
    {.channelCode = LEDC_CHANNEL_0, .used = false},
    {.channelCode = LEDC_CHANNEL_1, .used = false},
    {.channelCode = LEDC_CHANNEL_2, .used = false},
    {.channelCode = LEDC_CHANNEL_3, .used = false},
    {.channelCode = LEDC_CHANNEL_4, .used = false},
    {.channelCode = LEDC_CHANNEL_5, .used = false},
    {.channelCode = LEDC_CHANNEL_6, .used = false},
    {.channelCode = LEDC_CHANNEL_7, .used = false},
};

ledc_timer_config_t
initTimer(ledc_timer_bit_t res, uint32_t freq, ledc_mode_t mode,
          ledc_timer_t num) { // Initialize the timer for the LED controllers
  ledc_timer_config_t timer;
  timer.duty_resolution = res;
  timer.freq_hz = freq;
  timer.speed_mode = mode;
  timer.timer_num = num;

  ledc_timer_config(&timer);

  return timer;
}

LED::LED(ledc_timer_config_t timerConf, int _pin, double _correctionFactor) {
  led_channel channel1 = {.pin = _pin,
                          .controllerChannel = LEDC_CHANNEL_0,
                          .correctionFactor = _correctionFactor,
                          .color = 1};
  channel.push_back(channel1);
  timer = timerConf.timer_num;
  speedMode = timerConf.speed_mode;
  resolution = timerConf.duty_resolution;

  initLEDChannels();
  ESP_LOGV(TAG, "LED object created at pin %d", _pin);
}

LED::~LED() {
  // Mark used channels as unused
  for (int ch = 0; ch < channel.size(); ++ch) {
    for (int i = 0; i < 8; ++i)
      if (channel[ch].controllerChannel == channelOverview[i].channelCode) {
        channelOverview[i].used = false;
        break;
      }
  }
}

void LED::initLEDChannels() { // Set up the LED channels
  // Find free channels, mark them as used and initialize them
  for (int ch = 0; ch < channel.size(); ++ch) {
    int i;
    for (i = 0; i < 8; ++i)
      if (!channelOverview[i].used) {
        channel[ch].controllerChannel = channelOverview[i].channelCode;
        channelOverview[i].used = true;
        break;
      }
    // No free channel found
    if (i == 8)
      throw std::runtime_error("No free channels available");

    ledc_channel_config_t channel_conf;
    channel_conf.channel = channel[ch].controllerChannel;
    channel_conf.duty = 0;
    channel_conf.gpio_num = channel[ch].pin;
    channel_conf.speed_mode = speedMode;
    channel_conf.timer_sel = timer;

    ledc_channel_config(&channel_conf);
  }

  ledc_fade_func_install(0);
}

void LED::turnOn() { // Turn on the LED
  enabled = true;
  writeOutput();
  ESP_LOGV(TAG, "Switch on");
}

void LED::turnOff() { // Turn off the LED
  enabled = false;
  writeOutput();
  ESP_LOGV(TAG, "Switch off");
}

void LED::setEnabled(bool _enabled) {
  enabled = _enabled;
  writeOutput();
  ESP_LOGV(TAG, "Switch %s", _enabled ? "On" : "Off");
}

bool LED::toggle() { // Toggle status of the LED
  if (enabled) {
    turnOff();
  } else {
    turnOn();
  }
  return enabled;
}

void LED::writeOutput() { // Write the output to the hardware
  for (int ch = 0; ch < channel.size(); ++ch) {
    double duty =
        enabled * brightness * channel[ch].color * channel[ch].correctionFactor;
    ledc_set_fade_with_time(speedMode, channel[ch].controllerChannel,
                            duty * (pow(2, int(resolution)) - 1), fadeTime);
    ledc_fade_start(speedMode, channel[ch].controllerChannel,
                    LEDC_FADE_NO_WAIT);
  }
}

void LED::setBrightness(double _brightness) { // Set brightness of the LED
  if (_brightness != brightness) {
    brightness = _brightness;
    writeOutput();
  }
  ESP_LOGV(TAG, "Set brightness %lf", _brightness);
}

void LED::setFadeTime(int _fadeTime) { // Set fade time of the LED
  fadeTime = _fadeTime;
  ESP_LOGV(TAG, "Set fade time %dms", _fadeTime);
}

RGBLed::RGBLed(ledc_timer_config_t timerConf, int _pinRed, int _pinGreen,
               int _pinBlue, double _correctRed, double _correctGreen,
               double _correctBlue)
    : LED(timerConf, _pinRed, _correctRed) {
  led_channel channel2 = {.pin = _pinGreen,
                          .controllerChannel = LEDC_CHANNEL_0,
                          .correctionFactor = _correctGreen,
                          .color = 1};
  led_channel channel3 = {.pin = _pinBlue,
                          .controllerChannel = LEDC_CHANNEL_0,
                          .correctionFactor = _correctBlue,
                          .color = 1};

  channel.push_back(channel2);
  channel.push_back(channel3);

  initLEDChannels();
  ESP_LOGV(TAG, "Object created at pins %d,%d,%d", _pinRed, _pinGreen,
           _pinBlue);
}

bool LED::getEnabled() { return enabled; } // Get enabled state

void RGBLed::setColor(int hexColor,
                      bool enable) { // Set color via hexadecimal value
  enabled |= enable;
  channel[0].color = (hexColor % int(pow(16, 6)) - hexColor % int(pow(16, 4))) /
                     pow(16, 4) / (pow(16, 2) - 1);
  channel[1].color = (hexColor % int(pow(16, 4)) - hexColor % int(pow(16, 2))) /
                     pow(16, 2) / (pow(16, 2) - 1);
  channel[2].color = hexColor % int(pow(16, 2)) / (pow(16, 2) - 1);

  writeOutput();
  ESP_LOGV(TAG, "Set color %06x", hexColor);
}

void RGBLed::setColor(double red, double green, double blue,
                      bool enable) { // Set color via 0-1 values
  enabled = enable;
  channel[0].color = red;
  channel[1].color = green;
  channel[2].color = blue;

  writeOutput();
  ESP_LOGV(TAG, "Set color %lf/%lf/%lf", red, green, blue);
}
} // namespace LED
