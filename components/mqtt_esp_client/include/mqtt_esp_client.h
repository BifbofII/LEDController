/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    mqtt_esp_client.h  = */
/* =============================== */

#ifndef MQTT_ESP_CLIENT
#define MQTT_ESP_CLIENT

// ESP-IDF libraries
extern "C" {
#include "mqtt_client.h"
}
#include "esp_log.h"

// Own libraries
#include "mqtt_objects.h"

// C/C++ libraries
#include <algorithm>
#include <string>
#include <vector>

namespace MQTT {
class MqttClient {
protected:
  std::vector<const MQTT::MqttObject *> connectedObjects;
  esp_mqtt_client_config_t config;
  esp_mqtt_client_handle_t client;
  bool connected = false;

  void init(); // Always used initializations
  esp_err_t classEventHandler(esp_mqtt_event_handle_t event); // Event handler

public:
  MqttClient(std::string host, std::string clientID = "ESP32-MQTT",
             uint32_t port = 1883); // Constructor for normal TCP usage
  MqttClient(std::string host, std::string user, std::string password,
             std::string clientID = "ESP32-MQTT",
             uint32_t port = 1883); // Constructor for authenticated TCP usage
  ~MqttClient();
  void subscribe(std::string topic);   // Subscribe to a topic
  void unsubscribe(std::string topic); // Unsubscribe from a topic
  void publish(std::string topic,
               std::string data); // Publish a message to a topic
  void connectObject(
      const MQTT::MqttObject &object); // Connect an MqttObject to the client
  void
  disconnectObject(const MQTT::MqttObject &object); // Disconnect an MqttObject
  void disconnectObject(
      std::string topic); // Disconenct an MqttObject via it's topic
  bool getConnected();    // Get connection state

  friend esp_err_t eventHandler(esp_mqtt_event_handle_t event);
};
} // namespace MQTT

#endif
