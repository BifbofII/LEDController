/* ================================== */
/* = Author:  Christoph Jabs        = */
/* = Project: LEDController         = */
/* ================================== */
/* = File:    mqtt_client_class.cpp = */
/* ================================== */

#include "mqtt_esp_client.h"

namespace MQTT {
static const char *TAG = "MQTT-Client"; // Logging tag

static std::vector<MqttClient *> clientObjects; // List of all created clients

static esp_err_t
eventHandler(esp_mqtt_event_handle_t
                 event) { // Event handler to pass handling to client class
  for (int i = 0; i < clientObjects.size(); i++) {
    if (clientObjects[i]->client == event->client) {
      return clientObjects[i]->classEventHandler(event);
    }
  }
  ESP_LOGE(TAG, "No matching MQTT-Client for recieved event found");
  return ESP_FAIL;
}

MqttClient::MqttClient(std::string host, std::string clientID,
                       uint32_t port) { // Constructor for normal TCP usage

  sprintf(config.host, host.c_str());
  sprintf(config.client_id, clientID.c_str());

  init();
}

MqttClient::MqttClient(
    std::string host, std::string user, std::string password,
    std::string clientID = "ESP32-MQTT",
    uint32_t port = 1883) { // Constructor for authenticated TCP usage
  sprintf(config.host, host.c_str());
  sprintf(config.username, user.c_str());
  sprintf(config.password, password.c_str());
  sprintf(config.client_id, clientID.c_str());
  ESP_LOGI(TAG, "%s;%s;%s", config.host, config.username, config.password);

  init();
}

MqttClient::~MqttClient() {
  clientObjects.erase(
      std::find(clientObjects.begin(), clientObjects.end(), this));
}

void MqttClient::init() { // Always used initializations

  config.event_handle = eventHandler;

  client = esp_mqtt_client_init(&config);
  esp_mqtt_client_start(client);

  clientObjects.push_back(this);
}

esp_err_t
MqttClient::classEventHandler(esp_mqtt_event_handle_t event) { // Event handler

  std::string topic;
  std::string data;

  data.assign(event->data, event->data_len);
  topic.assign(event->topic, event->topic_len);

  switch (event->event_id) {
  case MQTT_EVENT_CONNECTED:
    ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
    connected = true;
    break;
  case MQTT_EVENT_DISCONNECTED:
    ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
    connected = false;
    break;
  case MQTT_EVENT_SUBSCRIBED:
    ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED");
    ESP_LOGV(TAG, "Subscribed to: %s", topic.c_str());
    break;
  case MQTT_EVENT_UNSUBSCRIBED:
    ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED");
    ESP_LOGV(TAG, "Unsubscribed from: %s", topic.c_str());
    break;
  case MQTT_EVENT_PUBLISHED:
    ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED");
    ESP_LOGD(TAG, "Published: Topic: %s\tData: %s", topic.c_str(),
             data.c_str());
    break;
  case MQTT_EVENT_DATA:
    ESP_LOGI(TAG, "MQTT_EVENT_DATA");
    ESP_LOGD(TAG, "Recieved: Topic: %s\tData: %s", topic.c_str(), data.c_str());

    // Pass event handling to corresponding connected object
    for (int i = 0; i < connectedObjects.size(); i++)
      if (topic.find(connectedObjects[i]->getTopic()) == 0)
        return connectedObjects[i]->eventHandler(topic, data);

    break;
  case MQTT_EVENT_ERROR:
    ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
    break;
  }
  return ESP_OK;
}

void MqttClient::subscribe(std::string topic) { // Subscribe to a topic
  esp_mqtt_client_subscribe(client, topic.c_str(), 0);
}

void MqttClient::unsubscribe(std::string topic) { // Unsubscribe from a topic
  esp_mqtt_client_unsubscribe(client, topic.c_str());
}

void MqttClient::publish(std::string topic,
                         std::string data) { // Publish a message to a topic
  esp_mqtt_client_publish(client, topic.c_str(), data.c_str(), 0, 0, 0);
}

void MqttClient::connectObject(
    const MQTT::MqttObject &object) { // Connect an MqttObject to the client
  connectedObjects.push_back(&object);
  subscribe(object.getTopic() + "/#");
}

void MqttClient::disconnectObject(
    const MQTT::MqttObject &object) { // Disconnect an MqttObject
  connectedObjects.erase(
      std::find(connectedObjects.begin(), connectedObjects.end(), &object));
  unsubscribe(object.getTopic() + "/#");
}

void MqttClient::disconnectObject(
    std::string topic) { // Disconnect an MqttObject via it's topic
  for (int i = 0; i < connectedObjects.size(); i++)
    if (connectedObjects[i]->getTopic() == topic) {
      connectedObjects.erase(connectedObjects.begin() + i);
      unsubscribe(topic + "/#");
      return;
    }
}

bool MqttClient::getConnected() { return connected; } // Get connection state
} // namespace MQTT
