/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    mqtt_objects.h     = */
/* =============================== */

#ifndef MQTT_OBJECTS
#define MQTT_OBJECTS

// Own libraries
#include "led_objects.h"
#include "string_utils.h"
#include "switch_objects.h"

// C/C++ libraries
#include <cmath>
#include <string>

namespace MQTT {
class MqttObject {
protected:
  std::string topic;

public:
  virtual esp_err_t
  eventHandler(std::string topic,
               std::string data) = 0; // Event handler to be overloaded
  void
  setTopic(std::string _topic); // Set the topic to which the object listens
  std::string getTopic();       // Get the topic to which the object listens
};

class MqttLED : public LED::LED, public MqttObject {
public:
  MqttLED(ledc_timer_config_t timerConf, int _pin,
          double _correctionFactor = 1);
  esp_err_t eventHandler(std::string topic, std::string data); // Event handler
};

class MqttRGBLed : public LED::RGBLed, public MqttObject {
public:
  MqttRGBLed(ledc_timer_config_t timerConf, int _pinRed, int _pinGreen,
             int _pinBlue, double _correctRed = 1, double _correctGreen = 1,
             double _correctBlue = 1);
  esp_err_t eventHandler(std::string topic, std::string data); // Event handler
};

class MqttSwitch : public SWITCH::Switch, public MqttObject {
public:
  MqttSwitch(int _pin);
  esp_err_t eventHandler(std::string topic, std::string data); // Event handler
};
} // namespace MQTT

#endif
