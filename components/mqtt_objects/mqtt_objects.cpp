/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    mqtt_objects.h     = */
/* =============================== */

#include "mqtt_objects.h"

namespace MQTT {
static const char *TAG = "MQTT-Object"; // Logging tag

void MqttObject::setTopic(
    std::string _topic) { // Set the topic to which the object listens
  topic = _topic;
}

std::string
MqttObject::getTopic() { // Get the topic to which the object listens
  return topic;
}

MqttLED::MqttLED(ledc_timer_config_t timerConf, int _pin,
                 double _correctionFactor)
    : LED(timerConf, _pin, _correctionFactor) {}

esp_err_t MqttLED::eventHandler(std::string topic,
                                std::string data) { // Event handler
  ESP_LOGV(TAG, "Reacting to topic: %s\t data: %s", topic.c_str(),
           data.c_str());
  if (topic == this->topic + "/en") {
    bool en = str2b(data);
    if (!std::isnan(en)) {
      setEnabled(en);
      return ESP_OK;
    }
  } else if (topic == this->topic + "/tog") {
    toggle();
    return ESP_OK;
  } else if (topic == this->topic + "/br") {
    setBrightness(str2d(data));
  } else if (topic == this->topic + "/ft") {
    int ft = str2i(data);
    if (!std::isnan(ft)) {
      setFadeTime(ft);
      return ESP_OK;
    }
  }
  return ESP_FAIL;
}

MqttRGBLed::MqttRGBLed(ledc_timer_config_t timerConf, int _pinRed,
                       int _pinGreen, int _pinBlue, double _correctRed,
                       double _correctGreen, double _correctBlue)
    : RGBLed(timerConf, _pinRed, _pinGreen, _pinBlue, _correctRed,
             _correctGreen, _correctBlue) {}

esp_err_t MqttRGBLed::eventHandler(std::string topic,
                                   std::string data) { // Event handler
  ESP_LOGV(TAG, "Reacting to topic: %s\t data: %s", topic.c_str(),
           data.c_str());
  if (topic == this->topic + "/en") {
    bool en = str2b(data);
    if (!std::isnan(en)) {
      setEnabled(en);
      return ESP_OK;
    }
  } else if (topic == this->topic + "/tog") {
    toggle();
    return ESP_OK;
  } else if (topic == this->topic + "/br") {
    double br = str2d(data);
    if (!std::isnan(br)) {
      setBrightness(br);
      return ESP_OK;
    }
  } else if (topic == this->topic + "/ft") {
    int ft = str2i(data);
    if (!std::isnan(ft)) {
      setFadeTime(ft);
      return ESP_OK;
    }
  } else if (topic == this->topic + "/col") {
    int col = hex2i(data);
    if (!std::isnan(col)) {
      setColor(col);
      return ESP_OK;
    }
  }
  return ESP_FAIL;
}

MqttSwitch::MqttSwitch(int _pin) : Switch(_pin) {}

esp_err_t MqttSwitch::eventHandler(std::string topic,
                                   std::string data) { // Event handler
  if (topic == this->topic + "/en") {
    bool en = str2b(data);
    if (!std::isnan(en)) {
      setEnabled(en);
      return ESP_OK;
    }
  } else if (topic == this->topic + "/tog") {
    toggle();
    return ESP_OK;
  } else if (topic == this->topic + "/pulse") {
    int t = str2i(data);
    if (!std::isnan(t)) {
      pulse(t);
      return ESP_OK;
    }
  }
  return ESP_FAIL;
}
} // namespace MQTT
