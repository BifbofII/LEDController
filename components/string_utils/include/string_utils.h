/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    string_utils.h     = */
/* =============================== */

#ifndef STRING_UTILS
#define STRING_UTILS

// C/C++ libraries
#include <limits>
#include <sstream>
#include <string>
#include <vector>

double str2d(const std::string &s); // Convert a string to a double
int str2i(const std::string &s);    // Convert a string to an integer
int hex2i(const std::string
              &s); // Convert a string to an integer (hex interpretation)
bool str2b(const std::string &s); // Convbert a string to a boolean
std::vector<std::string> splitStr(const std::string &s,
                                  char delim); // Split a string at delimeter

#endif
