/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    string_utils.cpp   = */
/* =============================== */

#include "string_utils.h"

double str2d(const std::string &s) { // Convert a string to a double
  std::istringstream is(s);
  double d;
  if (!(is >> d))
    return std::numeric_limits<double>::quiet_NaN();
  return d;
}

int str2i(const std::string &s) { // Convert a string to an integer
  std::istringstream is(s);
  int i;
  if (!(is >> i))
    return std::numeric_limits<int>::quiet_NaN();
  return i;
}

int hex2i(const std::string
              &s) { // Convert a string to an integer (hex interpretation)
  std::istringstream is(s);
  int i;
  if (!(is >> std::hex >> i))
    return std::numeric_limits<int>::quiet_NaN();
  return i;
}

bool str2b(const std::string &s) { // Convert a string to a boolean
  if (s == "ON" || s == "On" || s == "on" || s == "1" || s == "True" ||
      s == "true")
    return true;
  else if (s == "OFF" || s == "Off" || s == "off" || s == "0" || s == "False" ||
           s == "False")
    return false;
  else
    return std::numeric_limits<bool>::quiet_NaN();
}

std::vector<std::string> splitStr(const std::string &s,
                                  char delim) { // Split a string at delimeter
  std::vector<std::string> elements;
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim))
    elements.push_back(item);
  return elements;
}
