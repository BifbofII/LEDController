/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    switch_objects.h   = */
/* =============================== */

#ifndef SWITCH_OBJECTS
#define SWITCH_OBJECTS

// ESP-IDF libraries
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

namespace SWITCH {
class Switch {
protected:
  bool enabled = false;
  int pin;
  void writeOutput();

public:
  Switch(int _pin);
  void turnOn();                  // Turn on switch
  void turnOff();                 // Turn off switch
  void setEnabled(bool _enabled); // Set the enable state of the switch
  bool toggle();                  // Toggle status of the switch
  void pulse(int t = 500);        // Send a pulse of t ms
};
} // namespace SWITCH

#endif
