/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    switch_objects.cpp = */
/* =============================== */

#include "switch_objects.h"

namespace SWITCH {
static const char *TAG = "SWITCH";

Switch::Switch(int _pin) : pin(_pin) {
  gpio_pad_select_gpio(pin);
  gpio_set_direction(pin, GPIO_MODE_OUTPUT);
}

void Switch::writeOutput() {
  gpio_set_level(pin, enabled);
}

void Switch::turnOn() { // Turn on switch
  enabled = true;
  writeOutput();
  ESP_LOGV(TAG, "Switch on");
}

void Switch::turnOff() { // Turn off switch
  enabled = false;
  writeOutput();
  ESP_LOGV(TAG, "Switch off");
}

void Switch::setEnabled(bool _enabled) { // Set the enable state of the switch
  enabled = _enabled;
  writeOutput();
  ESP_LOGV(TAG, "Switch %s", _enabled ? "On" : "Off");
}

bool Switch::toggle() { // Toggle status of the switch
  if (enabled) {
    turnOff();
  } else {
    turnOn();
  }
  return enabled;
}

void Switch::pulse(int t) { // Send a pulse of t ms
  toggle();
  vTaskDelay(t / portTICK_PERIOD_MS);
  toggle();
}
} // namespace SWITCH
