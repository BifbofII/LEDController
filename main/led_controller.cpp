/* =============================== */
/* = Author:  Christoph Jabs     = */
/* = Project: LEDController      = */
/* =============================== */
/* = File:    led_controller.cpp = */
/* =============================== */

#include "led_controller.h"

static EventGroupHandle_t wifiEventGroup;
const static int CONNECTED_BIT = BIT0;

esp_err_t wifiEventHandler(void *ctx, system_event_t *event) {
  switch (event->event_id) {
  case SYSTEM_EVENT_STA_START:
    ESP_LOGI("WIFI", "SYSTEM_EVENT_STA_START");
    ESP_ERROR_CHECK(esp_wifi_connect());
    break;
  case SYSTEM_EVENT_STA_GOT_IP:
    ESP_LOGI("WIFI", "SYSTEM_EVENT_STA_GOT_IP");
    gpio_set_level(CONFIG_WIFI_STATUS_PIN, 1);
    ESP_LOGI("WIFI", "got ip:%s\n",
             ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
    xEventGroupSetBits(wifiEventGroup, CONNECTED_BIT);
    break;
  case SYSTEM_EVENT_STA_DISCONNECTED:
    ESP_LOGI("WIFI", "SYSTEM_EVENT_STA_DISCONNECTED");
    gpio_set_level(CONFIG_WIFI_STATUS_PIN, 0);
    xEventGroupClearBits(wifiEventGroup, CONNECTED_BIT);
    ESP_ERROR_CHECK(esp_wifi_connect());
    break;
  default:
    break;
  }
  return ESP_OK;
}

void wifiSetup() {
  gpio_pad_select_gpio(CONFIG_WIFI_STATUS_PIN);
  gpio_set_direction(CONFIG_WIFI_STATUS_PIN, GPIO_MODE_OUTPUT);
  gpio_set_level(CONFIG_WIFI_STATUS_PIN, 0);

  tcpip_adapter_init();
  wifiEventGroup = xEventGroupCreate();

  ESP_ERROR_CHECK(esp_event_loop_init(wifiEventHandler, NULL));

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

  wifi_config_t sta_config;
  sprintf(sta_config.sta.ssid, CONFIG_WIFI_SSID);
  sprintf(sta_config.sta.password, CONFIG_WIFI_PASSWORD);
  sta_config.sta.bssid_set = false;

  ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &sta_config));
  ESP_ERROR_CHECK(esp_wifi_start());

  xEventGroupWaitBits(wifiEventGroup, CONNECTED_BIT, false, true,
                      portMAX_DELAY);
}

extern "C" void app_main() {
  nvs_flash_init();
  wifiSetup();

  static MQTT::MqttClient mqttClient(CONFIG_MQTT_HOST, CONFIG_MQTT_USER,
                                     CONFIG_MQTT_PASSWORD,
                                     CONFIG_MQTT_CLIENT_ID);
  while (!mqttClient.getConnected())
    ; // Wait for the client to be connected

#ifdef CONFIG_RGB_LED
  ledc_timer_config_t timer = LED::initTimer();
  std::vector<std::string> pins = splitStr(CONFIG_RGB_PINS, ' ');
  static MQTT::MqttRGBLed rgbLed(timer, str2i(pins[0]), str2i(pins[1]),
                                 str2i(pins[2]));
  rgbLed.setTopic(CONFIG_MQTT_RGB_TOPIC);
  mqttClient.connectObject(rgbLed);
#endif

#ifdef CONFIG_RELAY_BOARD
  pins = splitStr(CONFIG_RELAY_BOARD_PINS, ' ');
  static MQTT::MqttSwitch relay1(str2i(pins[0]));
  static MQTT::MqttSwitch relay2(str2i(pins[1]));
  static MQTT::MqttSwitch relay3(str2i(pins[2]));
  std::vector<std::string> topics = splitStr(CONFIG_MQTT_RELAY_TOPICS, ' ');
  relay1.setTopic(topics[0]);
  relay2.setTopic(topics[1]);
  relay3.setTopic(topics[2]);
  mqttClient.connectObject(relay1);
  mqttClient.connectObject(relay2);
  mqttClient.connectObject(relay3);
#endif
}
